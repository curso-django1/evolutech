## OpenBackend

**OpenStack** Backend Project
### Tecnologías

  * [Django](https://www.djangoproject.com/)
  * [Webmin](https://www.djangoproject.com/)


### Crear Base de Datos en postgres

  - `sudo su postgres`
  - `psql -c "DROP DATABASE chat_app1"`
  - `psql -c "DROP USER chat_user1"`
  - `psql -c "CREATE USER chat_user1 WITH ENCRYPTED PASSWORD 'XXXYYYZZZ'"`
  - `psql -c "CREATE DATABASE chat_app1 WITH OWNER chat_user1"`


### Crear Install redis server / Django Constance

	- sudo apt-get install redis-server

DevZone es soportado por [@alfredynho](alfredynho.cg@gmail.com).


python manage.py seed category --settings=settings.config.development --number=100 

python manage.py seed category --number=15

pip install django-seed


https://www.appsscript.it/articoli/conflict-can-t-use-getupdates-method-while-webhook-is-active/
