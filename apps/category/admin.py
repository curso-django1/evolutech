from django.contrib import admin
from .models import Categoria

@admin.register(Categoria)
class AdminCategoria(admin.ModelAdmin):
    
    list_display = ["title"]
    search_fields = ["title"]
    
    list_display = [
        'id',
        'name',
        'slug',
        'publish',
    ]
    
    class Model:
        model = Categoria