# >> Django Version
# --------------------
Django==4.1.1

# # >> Django Admin
django-admin-interface==0.20.0
django-colorfield==0.7.2
django-flat-responsive==2.0
django-flat-theme==1.1.4

# >> Images
# --------------------
Pillow==9.2.0

# >> DataBases
# --------------------
psycopg2==2.9.3

# >> Time Zone
# --------------------
# pytz==2018.3


# >> Django Rest
# --------------------
djangorestframework==3.14.0


# >> Django Debug Toolbar
# --------------------
django-debug-toolbar==3.6.0

# >> Django Constance
# --------------------
django-constance==2.9.1   

# >> Requests
# --------------------
requests==2.28.1

# >> Celery
# --------------------
celery==5.2.7
