from argparse import Namespace
from django.contrib import admin
from django.urls import include, path
from django.contrib import admin
from django.views.generic import TemplateView
from django.views.static import serve
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, re_path
import debug_toolbar

# from django.contrib.auth.views import (
#         login, 
#         logout_then_login, 
#         password_reset, 
#         password_reset_done, 
#         password_reset_confirm, 
#         password_reset_complete
#     )


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^', include(('apps.home.urls','index'),namespace='index')),
    re_path(r'^', include(('apps.category.urls','category'),namespace='category')),
    re_path(r'^', include(('apps.post.urls','post'),namespace='post')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    import debug_toolbar

    urlpatterns +=(
        re_path(r'^__debug__/', include(debug_toolbar.urls)),
    )